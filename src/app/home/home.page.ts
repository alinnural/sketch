import { Component } from '@angular/core';
import { parse } from 'querystring';

declare var p5: any;

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor() {}

  width:string;
  height:string;
  length:string;
  result:number;

  ngOnInit() {
    document.getElementById('p5sketch').innerHTML = "";
    const sketch = (s) => {

      s.preload = () => {
        // preload code
      }

      s.setup = () => {
        s.createCanvas(screen.width, screen.height);
      };
      
      s.windowResized = () => {
        s.resizeCanvas(screen.width, screen.height);
      }

      s.draw = () => {
        let firstCord = 50;

        s.background(51);
        s.noFill();

        //main shape
        s.stroke(255);
        s.rect(firstCord, firstCord, this.width, this.length);
        s.quad(firstCord, firstCord, 
              firstCord+parseInt(this.width), firstCord,
              firstCord+parseInt(this.width)-(parseInt(this.height)/2), firstCord+(parseInt(this.length)/2),
              firstCord+(parseInt(this.height)/2), firstCord+(parseInt(this.length)/2)
              );
        s.quad(firstCord+parseInt(this.width)-(parseInt(this.height)/2), firstCord+(parseInt(this.length)/2),
              firstCord+(parseInt(this.height)/2), firstCord+(parseInt(this.length)/2),
              firstCord, firstCord+parseInt(this.length),
              firstCord+parseInt(this.width), firstCord+parseInt(this.length)
              );
        
        s.stroke(255, 204, 0);
        let changeY = 10+firstCord+parseInt(this.length);
        s.quad(firstCord+parseInt(this.width)-(parseInt(this.height)/2), changeY+(parseInt(this.length)/2),
              firstCord+(parseInt(this.height)/2), changeY+(parseInt(this.length)/2),
              firstCord, changeY+parseInt(this.length),
              firstCord+parseInt(this.width), changeY+parseInt(this.length)
              );

        //triangle shape
        let changeX = 50;
        s.triangle(changeX+firstCord+parseInt(this.width)-(parseInt(this.height)/2), firstCord+(parseInt(this.length)/2),
                  changeX+firstCord+parseInt(this.width), firstCord,
                  changeX+firstCord+parseInt(this.width), firstCord+parseInt(this.length)
                  );
      };
    }

    let myp5 = new p5(sketch, document.getElementById('p5sketch'));
  }
  
}
